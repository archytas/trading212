<?php


namespace Archytas\Trading212;


use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;

class ApiClient
{
    protected $client;
    protected $user;
    protected $accountId;
    protected $initInfo;
    protected $translations;

    /**
     * ApiClient constructor.
     * @param Client|null $client
     * @param User|null $user
     */
    public function __construct(Client $client = null, User $user = null)
    {
        Log::info('construct Trading212 API');

        if (!isset($client)) {
            $client = new Client([
                'base_uri' => 'https://live.trading212.com/rest/v1/',
                'cookies' => true,
                'verify' => false,
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
                'Host' => 'live.trading212.com',
                'Referer' => 'https://live.trading212.com/'
            ]);
        }
        if (!isset($user)) {
            $user = \Auth::user();
        }

        $this->client = $client;
        $this->user = $user;
    }

    /**
     * @param $username
     * @param $password
     * @throws \Exception
     */
    public function login($username, $password)
    {
        $r = $this->client->request('GET', 'https://www.trading212.com/en/login',
            [
                'verify' => false,
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
            ]);
        $contents = $r->getBody()->getContents();
        $dc = new Crawler($contents);
        $token = $dc->filter('[name="login[_token]"]')->first()->attr('value');

        $response = $this->client->request('POST', 'https://www.trading212.com/nl/authenticate', [
            'form_params' => [
                'login[username]' => $username,
                'login[password]' => $password,
                'login[rememberMe]' => '1',
                'login[_token]' => $token
            ]
        ]);

        $cookieJar = $this->client->getConfig('cookies');

        try {
            $response = $this->client->request('GET', 'https://live.trading212.com/',
                [
                    'headers' => [
                        'Referer' => 'https://live.trading212.com/',
                        'Accept-Language' => 'nl,en-US;q=0.9,en;q=0.8,fr;q=0.7,es;q=0.6,de;q=0.5',
                    ]
                ]);
        } catch (\Exception $e) {
            Log::warning($e->getMessage());
        }

        $contents = $response->getBody()->getContents();
        $lines = explode("\n", $contents);

        $accountId = '';
        foreach ($lines as $line) {
            if (strpos($line, 'accountId') !== false) {
                $line = trim($line);
                $line = str_replace(['\'accountId\': \'', '\','], '', $line);
                $accountId = $line;
            }
        }

        if (!is_numeric($accountId)) {
            throw new \Exception('Failed to fetch accountId');
        }

        $this->accountId = $accountId;
    }

    public function getInitInfo()
    {
        $traderClient = 'application=WC4, version=5.100.9, accountId=' . $this->accountId;

        try {
            $response = $this->client->request('GET', 'https://live.trading212.com/rest/v3/init-info',
                [
                    'headers' => [
                        'X-Trader-Client' => $traderClient,
//                        'X-Trader-Id' => $traderId,
                        'X-Trader-SeqId' => '0',
                        'Sec-Fetch-Site' => 'same-origin',
                        'Sec-Fetch-Mode' => 'cors',
                        'Sec-Fetch-Dest' => 'empty',
                        'Accept' => 'application/json',
                        'Referer' => 'https://live.trading212.com/',
                        'Accept-Language' => 'nl,en-US;q=0.9,en;q=0.8,fr;q=0.7,es;q=0.6,de;q=0.5',
                    ]
                ]);
        } catch (\Exception $e) {
            // oops
        }

        $data = $response->getBody()->getContents();
        $this->initInfo = json_decode($data, false);

        // $data->account->positions
        return $this->initInfo;
    }

    /**
     * @return mixed|null
     */
    public function getPositions()
    {
        if (!isset($this->initInfo)) {
            $this->getInitInfo();
        }
        return optional($this->initInfo->account)->positions;
    }

    public function getPositionNameByCode(string $code) :  string
    {
        if (!isset($this->translations)) {
            $this->translations = file_get_contents(__DIR__ . DS . 'translations' . DS . 'trader.js');
            $this->translations = json_decode($this->translations, false);
        }

        $symbol = $this->getSymbolByCode($code);
        $key = 'instrument.' . $symbol . '.name';
        return $this->translations->$key ?? '';
    }

    public function getSymbolByCode(string $code) :  string
    {
        $parts = explode('_', $code);
        return $parts[0];
    }
}