<?php

namespace Archytas\Trading212;

use App\DeGiroApi\DeGiroApi;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class Trading212ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes/web.php';
        $this->app->make('Archytas\Trading212\PortfolioController');

        $this->app->bind('trading212-api', function ($app) {
            $client = new Client([
                'base_uri' => 'https://live.trading212.com/rest/v1/',
                'cookies' => true,
                'verify' => false,
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
                'Host' => 'live.trading212.com',
                'Referer' => 'https://live.trading212.com/'
            ]);

            return new ApiClient($client, \Auth::user());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
