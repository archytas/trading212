<?php

namespace Archytas\Trading212;

use Illuminate\Support\Facades\Facade;

class Trading212Facade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'trading212-api';
    }
}